var appRouter = function(app) {
    //
    var mysql = require("mysql");
    var cors = require("cors");
    var con_Remote = mysql.createConnection({
        host: "127.0.0.1",
        user: "root",
        port: "3306",
        password: "root",
        database: "myDB",
        charset: "utf8"
    });

    var con_LOCAL = mysql.createConnection({
        host: "127.0.0.1",
        user: "root",
        port: "8889",
        password: "root",
        database: "connect_production",
        charset: "utf8"
    });

    var con = con_Remote;
    /*
   "username": "root",
      "password": "root",
      "database": "connect_production",
      "host": "127.0.0.1",
      "dialect": "mysql",
      "port":"8889",
      "timezone":"America/New_York"
      */
    ///api.js
    /*
What is important here is that we named our database host 
as ‘database’. This link is in fact described later on in 
our docker-compose.yml file in which we will link the 
database service to the backend service so the backend 
will be able to interact as we described.
*/
    // initial connection
    con.connect(function(err) {
        if (err) console.log(err);
    });
    //
    app.get("/", function(req, res) {
        res.status(200).send("Welcome to our restful API");
    });

    app.get("/monkey", function(req, res) {
        res.status(200).send("no more monkeys jumping on the bed.");
    });

    // our simple get /jobs API
    app.get("/bugs", (req, res) => {
        con.query("SELECT * FROM connect_bugs.bugs", function(
            err,
            result,
            fields
        ) {
            if (err) res.send(err);
            res.send(result);
            console.log(result);
        });
    });

    // our simple get /jobs API
    app.get("/todos", (req, res) => {
        con.query("SELECT * FROM connect_bugs.todos", function(
            err,
            result,
            fields
        ) {
            if (err) res.send(err);
            res.send(result);
            console.log(result);
        });
    });
    /*
    // our simple get /jobs API
    app.get("/jobs", (req, res) => {
        con.query("SELECT * FROM myDB.jobs", function(err, result, fields) {
            if (err) res.send(err);
            res.send(result);
            console.log(result);
        });
    });
    */
};

module.exports = appRouter;
