var express = require("express");
var bodyParser = require("body-parser");
var routes = require("./routes/routes.js");
var cors = require("cors");
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
 
routes(app);

var server = app.listen(3004, function () {
    console.log("app running on port.", server.address().port);
});

///api.js
/*
What is important here is that we named our database host as ‘database’. This link is in fact described later on in our docker-compose.yml file in which we will link the database service to the backend service so the backend will be able to interact as we described.
*/
/*
const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const cors = require('cors');
var con = mysql.createConnection({
    host: "database",
    user: "root",
    port: '3306',
    password: "somePassword",
    database: "mean",
    charset  : 'utf8'
});
var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
}
// initial connection
con.connect(function(err) {
    if(err) console.log(err);
});
// our simple get /jobs API
router.get('/jobs', cors(corsOptions), (req, res) => {
    con.query("SELECT * FROM jobs", function (err, result, fields) {
        if (err) res.send(err);
        res.send(result);
        console.log(result);
    });
});

module.exports = router;
*/